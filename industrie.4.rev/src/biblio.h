// #include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
// #include <AsyncTCP.h>
#include "SPIFFS.h"
#include "Time.h"
// #include "RTClib.h"
// #include <TimeLib.h>
// #include <WiFiClientSecure.h> // pour etablir une connexion securise avec un serveur distant 
// #include <WiFiClient.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>


HTTPClient http;

// Create AsyncWebServer object on port 80
AsyncWebServer serverLocal(80);
// WiFiServer server(80);
const char*  serverRemote = "https://192.168.1.113:8000/api/table_journales";  // Server URL
const char* test_root_ca= \
"-----BEGIN CERTIFICATE-----\n" \
"MIIEeDCCAuCgAwIBAgIRAPdXxl0HvF68/e/b0V62Ed0wDQYJKoZIhvcNAQELBQAw\n" \
"gYUxFzAVBgNVBAoTDlN5bWZvbnkgZGV2IENBMTAwLgYDVQQLDCdERVNLVE9QLThP\n" \
"SEI2M0VcTmF3ZmVsZEBERVNLVE9QLThPSEI2M0UxODA2BgNVBAMML1N5bWZvbnkg\n" \
"REVTS1RPUC04T0hCNjNFXE5hd2ZlbGRAREVTS1RPUC04T0hCNjNFMB4XDTIzMDgx\n" \
"NzE2MjM1MVoXDTI1MTExOTE2MjM1MVowYTEZMBcGA1UEChMQU3ltZm9ueSBkZXYg\n" \
"Y2VydDEwMC4GA1UECwwnREVTS1RPUC04T0hCNjNFXE5hd2ZlbGRAREVTS1RPUC04\n" \
"T0hCNjNFMRIwEAYDVQQDEwlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IB\n" \
"DwAwggEKAoIBAQDu1XVAbWbOUdbK5btCDYoncYFRtb/U13MBa9iVG3DKssGEWzGt\n" \
"6pfhpMZYP9TM5siL0Xp+Htmyr8IGf2IqeMfRPbDF12GT9UvHWzJv/CFZX/jAlYt1\n" \
"UhSpKm7j9GGBKLIxvX9tPmGMa9bU4QiEhuhZU1nJ6VvxBEv+5Gi4ZRL/W3YR/7Hs\n" \
"xQoNcWUums6ERwCo0TSessGnNdeeO/VQf8D8J7BzK2b7yiMEXOFOY+YxXLjArRQM\n" \
"lgv5vTvEotqtmGmTzv4Ox8WhQqhis8flOSmYGQMX/3fxVYfilw9zn9wZ9tISF2wy\n" \
"azX3V2W6Okaxzjq3Bk5k3UXhMkVvLPA3ZuzLAgMBAAGjgYUwgYIwDgYDVR0PAQH/\n" \
"BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAwGA1UdEwEB/wQCMAAwHwYDVR0j\n" \
"BBgwFoAUnJ0S1CxLC3EJyI+hAoz2K4Si+K4wLAYDVR0RBCUwI4IJbG9jYWxob3N0\n" \
"hwR/AAABhxAAAAAAAAAAAAAAAAAAAAABMA0GCSqGSIb3DQEBCwUAA4IBgQCSyHny\n" \
"ARU7eZUJw7M1V4L3KDi7qI6y8zQzXLOkwr5Zqm59vxKkMGbxWV+/CPr10zlxcCET\n" \
"25v2b2Xee7uyoK4l+VByHTcoOqJ3vEeRmb+KlyXsihUqNCYjrcdHjRCXa9LQd365\n" \
"B43dClmMOo4TR56/aNoBeSgPXLNBwCdtWvXTj7h0Q3G9uy2YF4nKrRurt00lblEO\n" \
"17pImXqzOlL7EOoAf8I6XG4kQxNUxHziZP+ZYvcozcp/nPGbl/dIsXwmTNi05chD\n" \
"FTgYiqcGSC1lSww+pUIeuFxzAW84zroHjElH+IavEiZFK78o1PdRCA2bzxJq46tx\n" \
"Tl4I6SF9n7ZTBKwxauNB5glYRCCUE/GYXQ5SuGQNRmxyVd64bNwcUPqRKQl9sDwV\n" \
"egKEyQhULGaNjcC4aia5rj4Neti7p17oDfMH4pmqPauvi21/4hUZ8CjYeWMq3B8m\n" \
"iGa9KrhoOWDywGPEZUkMWOiFUv97D1UwnHJ4Rkz/AUqGdDLXirLIYybSqIY=\n" \
  "-----END CERTIFICATE-----\n"; 
char  jsonOutput[128];
WiFiClientSecure client;
// Search for parameter in HTTP POST request
const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
const char* PARAM_INPUT_3 = "ip";
const char* PARAM_INPUT_4 = "gateway";
const char* PARAM_INPUT_5 = "nameEsp";
const char* PARAM_INPUT_6 = "adresseServer";
const char* PARAM_INPUT_7 = "timesending";

bool sensorRead = true;
int n = 0;
String nameStructNow;
// Variable to store the HTTP request
String header;
//Variables to save values from HTML form
String ssid;
String pass;
String ip;
String gateway;
String nameEsp;
String serverWeb;
String sendingTime;
String ledState;

struct dataToApi{
   String machineName;
   String dateheure;
   String pointnumber;
   String steptime;
   int packetTime;
   int awakeSince;
   String rowinarray;
};

dataToApi DataToApis[720];
// variables to Rotate detector
int sensorPin = 15; // pin OUT from sensor
int sensorValue = 0; // initialisation variable counter
int sensorValuePast = 0; // past value of sensor
// File paths to save input values permanently
const char* ssidPath = "/ssid.txt";
const char* passPath = "/pass.txt";
const char* ipPath = "/ip.txt";
const char* gatewayPath = "/gateway.txt";
const char* nameEspPath = "/nameEsp.txt";
const char* serverWebPath = "/serverWeb.txt";
const char* sendingTimePath = "/sendingTime.txt";


unsigned long Epoch_Time; 

// Get_Epoch_Time() Function that gets current epoch time
unsigned long Get_Epoch_Time() {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    //Serial.println("Failed to obtain time");
    return(0);
  }
  time(&now);
  return now;
}


// Timer variables
unsigned long previousMillis = 0;
const long interval = 10000;  // interval to wait for Wi-Fi connection (milliseconds)
// config serveur temp
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;


// Set LED GPIO
const int ledPin = 2;
// Stores LED state
hw_timer_t * timer = NULL;
volatile uint8_t led_state = 0;
int timerCount = 0;
int counter = 0;

struct Detector {
	const uint8_t PIN;
	uint32_t numberrotates;
	bool rotate;
}; 
Detector detecteur = {15, 0, false};

void IRAM_ATTR detector_isr() {
	detecteur.numberrotates++;
	detecteur.rotate = true;
}


void IRAM_ATTR timer_isr(){
  timerCount = timerCount +1;
  counter = counter +1;
  led_state = ! led_state;
  digitalWrite(ledPin, led_state);
}

// Initialize SPIFFS
void initSPIFFS() {
  if (!SPIFFS.begin(true)) {
    Serial.println("An error has occurred while mounting SPIFFS");
  }
  Serial.println("SPIFFS mounted successfully");
}

// Read File from SPIFFS
String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if(!file || file.isDirectory()){
    Serial.println("- failed to open file for reading");
    return String();
  }
  
  String fileContent;
  while(file.available()){
    fileContent = file.readStringUntil('\n');
    break;     
  }
  return fileContent;
}

// Write file to SPIFFS
void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
}


// Replaces placeholder with LED state value
String processor(const String& var) {
  if(var == "STATE") {
    if(digitalRead(ledPin)) {
      ledState = "ON";
    }
    else {
      ledState = "OFF";
    }
    return ledState;
  }
  return String();
}

void funcServeurLocal()
{
// serveur web au demarrage
        // Connect to Wi-Fi network with SSID and password
    Serial.println("Setting AP (Access Point)");
    // NULL sets an open Access Point
    WiFi.softAP("ESP-WIFI-MANAGER", NULL);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP); 

    // Web Server Root URL
    serverLocal.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(SPIFFS, "/wifimanager.html", "text/html");
    });
    
    serverLocal.serveStatic("/", SPIFFS, "/");
    
    serverLocal.on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
      int params = request->params();
      for(int i=0;i<params;i++){
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_1) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            writeFile(SPIFFS, ssidPath, ssid.c_str());
          }
          // HTTP POST pass value
          if (p->name() == PARAM_INPUT_2) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save value
            writeFile(SPIFFS, passPath, pass.c_str());
          }
          // HTTP POST ip value
          if (p->name() == PARAM_INPUT_3) {
            ip = p->value().c_str();
            Serial.print("IP Address set to: ");
            Serial.println(ip);
            // Write file to save value
            writeFile(SPIFFS, ipPath, ip.c_str());
          }
          // HTTP POST gateway value
          if (p->name() == PARAM_INPUT_4) {
            gateway = p->value().c_str();
            Serial.print("Gateway set to: ");
            Serial.println(gateway);
            // Write file to save value
            writeFile(SPIFFS, gatewayPath, gateway.c_str());
          }
          // HTTP POST nameEsp value
          if (p->name() == PARAM_INPUT_5) {
            nameEsp = p->value().c_str();
            Serial.print("Name ESP set to: ");
            Serial.println(nameEsp);
            // Write file to save value
            writeFile(SPIFFS, nameEspPath, nameEsp.c_str());
          }
          // HTTP POST serverWeb value
          if (p->name() == PARAM_INPUT_6) {
            serverWeb = p->value().c_str();
            Serial.print("serverWeb set to: ");
            Serial.println(serverWeb);
            // Write file to save value
            writeFile(SPIFFS, serverWebPath, serverWeb.c_str());
          }
          // HTTP POST sendingTime value
          if (p->name() == PARAM_INPUT_7) {
            sendingTime = p->value().c_str();
            Serial.print("sendingTime set to: ");
            Serial.println(sendingTime);
            // Write file to save value
            writeFile(SPIFFS, sendingTimePath, sendingTime.c_str());
          }                              
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      request->send(200, "text/plain", "Done. ESP will restart, connect to your router and go to IP address: " + ip);
      delay(3000);
      ESP.restart();
    });
    serverLocal.begin();

}


void funcReadanShow()
{
  // Load values saved in SPIFFS
  ssid = readFile(SPIFFS, ssidPath);
  pass = readFile(SPIFFS, passPath);
  ip = readFile(SPIFFS, ipPath);
  gateway = readFile (SPIFFS, gatewayPath);
  nameEsp = readFile (SPIFFS, nameEspPath);
  serverWeb = readFile (SPIFFS, serverWebPath);
  sendingTime = readFile (SPIFFS, sendingTimePath);
  Serial.println(ssid);
  Serial.println(pass);
  Serial.println(ip);
  Serial.println(gateway);
  Serial.println(nameEsp);
  Serial.println(serverWeb);
  Serial.println(sendingTime);
}


// function du sensor elle test et incremente counter
void funcSensor()
{
  if (sensorRead && digitalRead(sensorPin))
    {
      sensorValue = sensorValue + digitalRead(sensorPin);
      sensorRead = false;
      Serial.println(sensorValue);
    }
    
    if (!sensorRead && !digitalRead(sensorPin))
    {
      sensorRead = true;
    }

    if (detecteur.rotate) {
    Serial.printf("Rotation revolu %u tours\n", detecteur.numberrotates);
    detecteur.rotate = false;
    }
}