#include "biblio.h"

// const char *ssid2 = "Orange-70AA";
// const char *password2 = "LNGt7bqMHT6";

void setup() {
  // configurer le pin pour entree c'est le GPIO 15 
  pinMode(detecteur.PIN, INPUT);
  // Serial port for debugging purposes
  Serial.begin(115200);
    // Set GPIO 2 as an OUTPUT
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
	attachInterrupt(detecteur.PIN, detector_isr, FALLING);
  uint8_t timer_id = 0;
  uint16_t prescaler = 80;
  int threashold = 1000000;
  timer = timerBegin(timer_id, prescaler, true);
  timerAttachInterrupt(timer, &timer_isr, true);
  timerAlarmWrite(timer, threashold, true);
  timerAlarmEnable(timer);

  initSPIFFS();
  configTime(0, 0, ntpServer);

  funcReadanShow();

  if(ssid=="" || ip==""){
    Serial.println("Undefined SSID or IP address.");
    funcServeurLocal();
    Serial.println("Not ssd wait...");
    delay(120000);
    }
  // client.setCACert(test_root_ca);


  funcServeurLocal();
  Serial.println("waiting for config!!");
  delay(1200);	
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
	Serial.print("Connecting to WiFi");
	while (WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(500);
	}
	
	Serial.println("\nConnected to the WiFi network");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
}

void loop() {
  // read sensor value
  funcSensor();

  // if (counter >= sendingTime.toInt())
  if (counter >= 10)
  {
    Serial.println("**");
    Serial.print("nombre de n  ");
    Serial.println(String(n));    
    Serial.println("**");

    Serial.print(timerCount);
    Serial.print("**");
    Serial.print(counter);
    Serial.print("**");
    Serial.println(detecteur.numberrotates);

    Epoch_Time = Get_Epoch_Time();
    Serial.print("Epoch Time: ");
    Serial.println(Epoch_Time);
    nameStructNow = "ToApi_" + String(n) ;
    Serial.println("name struct  " + nameStructNow);
    DataToApis[n] = {nameEsp, String(Epoch_Time), String(detecteur.numberrotates), sendingTime, counter, timerCount, String(n)};

    // String number = nameStructNow.substring(6);
    Serial.println("print contenue du tableau des structure   .........");
    for (size_t i = 0; i <= n; i++)
    {
      Serial.println("machineName    " +DataToApis[n].machineName);
      Serial.println("dateheure      " +DataToApis[n].dateheure);
      Serial.println("packetTime     " +DataToApis[n].packetTime);
      Serial.println("awakeSince     " +DataToApis[n].awakeSince);
      Serial.println("pointnumber    " +DataToApis[n].pointnumber);
      Serial.println("steptime       " +DataToApis[n].steptime);
      Serial.println("rowinarray     " +DataToApis[n].rowinarray);
    }
    

    if((WiFi.status() == WL_CONNECTED))
      {
        for (size_t i = 0; i <= n; i++)
        {
          HTTPClient client;
          client.begin(serverWeb);
          client.addHeader("Content-Type", "application/json");
          
          const size_t CAPACITY = JSON_OBJECT_SIZE(10);
          StaticJsonDocument<CAPACITY> doc;
          
          JsonObject object = //doc.to<JsonObject>();
          doc["machineName"] = DataToApis[i].machineName;
          doc["dateheure"] = String(DataToApis[i].dateheure);
          doc["pointnumber"] = String(DataToApis[i].pointnumber);
          doc["steptime"] = String(DataToApis[i].steptime);
          doc["packetTime"] = DataToApis[i].packetTime;
          doc["awakeSince"] = DataToApis[i].awakeSince;
          doc["rowinarray"] = DataToApis[i].rowinarray;
          
          
          counter = 0;
          
          serializeJson(doc, jsonOutput);
          int httpCode = client.POST(String(jsonOutput));
          
          if(httpCode > 0) {
            String payload = client.getString();
            Serial.println("\nStatuscode: " + String(httpCode));
            Serial.println("\n I du for  : " + String(i));
            if (httpCode == 500)
            {
              if (n > 0)
              {
                n--;
              }
            }
            else
            {
              n++;
            }
            // Serial.println(payload);
            client.end();
          }
          else{
            Serial.println("Error on HTTP request");
            n++;
          }
        }
      }
      else {
        Serial.println("Connection lost");
      }
  }
}
